package mx.leo.testrappi.base.usecase

/**
 * Created by Leo on 13/05/17.
 */
abstract class UseCase<P,R> {
    abstract fun execute(params:P?= null, success:(R) -> Unit, error : (Error) -> Unit)
}