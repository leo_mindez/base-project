package mx.leo.testrappi

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import mx.leo.testrappi.base.data.net.NetManager

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
